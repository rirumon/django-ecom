from django.urls import path
from home import views
urlpatterns = [
    path('',views.home,name='home'),
    path('products',views.product,name="products"),
    path('product',views.single_product, name='single_product'),
    path('cart',views.cart, name='cart'),
    path('wishlist',views.wishlist),
    path('register',views.register),
    path('login',views.login)
]