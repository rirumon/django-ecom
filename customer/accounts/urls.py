from django.urls import path
from accounts import views

urlpatterns = [
    path('', views.home, name="home"),
    path('products', views.products, name="products"),
    path('customers/<int:pk>/', views.customers, name="customers"),
    path('order/place/<int:pk>', views.create_order, name="order_create"),
    path('order/update/<int:id>', views.update_order, name="order_update"),
    path('order/delete/<int:id>', views.order_delete, name="order_delete"),
]
