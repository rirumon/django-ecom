import django_filters
from django_filters import DateFilter
from .models import Order


class OrderFilter(django_filters.FilterSet):
    # start_date = DateFilter(field_name='create_at', lookup_expr="gta")
    # end_date = DateFilter(field_name='create_at', lookup_expr="lte")

    # exclude = ['customer', 'create_at']
    class Meta:
        models = Order
        fields = ['product','status']
