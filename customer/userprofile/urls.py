from django.urls import path, include
from userprofile import views

urlpatterns = [
    path('', views.profile, name="profile"),
    path('login', views.userLogin, name='login'),
    path('register', views.userRegister, name='register')
]
